var config_path = process.env.LIB_CONFIG_PATH || '';


if (config_path.length > 0 && config_path != __filename) {
  module.exports = require(config_path);
}
else {
  var _config = module.exports = {};
  var MySQL_Master = {host: '127.0.0.1', port: 3306, user: 'developer', password: '123456'};
  var MySQL_Slave = [
    {host: '127.0.0.1', port: 3306, user: 'developer', password: '123456'},
    {host: '127.0.0.1', port: 3306, user: 'developer', password: '123456'}
  ];
  const Redis_Tracking = { 
    sentinels: [
      {host: 'xx.xx.xx.xx', port: 2679},  // RandFish
      {host: 'xx.xx.xx.xx', port: 2679}, // Test2
    ], 
    dbNumber: 1
  };
  var Redis_Master = {host: '172.0.0.1', port: 6379};
  var Redis_Slave = {host: '172.0.0.1', port: 6379};
  var Redis_Sentinels = [
    {host: 'xx.xx.xx.xx', port: 26379},  // 
    {host: 'xx.xx.xx.xx', port: 26379}, // Test2
     
  ];
  const TIME_ZONE_MYSQL = '+07:00';
  // const Mongo_Master = {host: 'xx.xx.xx.xx', port: 27017, user: 'developer', password: 'edN3I5LISqv1tTIOrK'};
  // const Mongo_Slave = {host: 'xx.xx.xx.xx', port: 27017, user: 'developer', password: 'edN3I5LISqv1tTIOrK'};

  // var RabbitMQ_Master = {host: 'xx.xx.xx.xx', user: 'rabbituser', password: '11111'};
  // var RabbitMQ_Slave = {host: 'xx.xx.xx.xx', user: 'rabbituser', password: '11111'};

  // Cluster: RandFish, Pserver1, Pserver2
  var ES_Master = {host: ["192.168.1.161:9200"]}; //103.97.124.31:7200
  var ES_Slave = { host: ["192.168.1.161:9200"] };
  const ES_Query_V7 = { host: [
    "http://192.168.1.16:9200",
    "http://192.168.1.16:9201",
    "http://192.168.1.16:9202"
  ]
};
  _config.prefix_account = "CD";
  _config.secrectkey = 'QWRR#%@#^@#RR@';
  _config.hasPass = 'XbKT$%Ro';
  _config.time_logout = 5;
  _config.time_type = 'minutes'; //hours
  _config.SYSTEM = {
    STATUS_TRACKING: true,
    ENABLE: true,
    CONFIG_PATH: __filename,
    //VARNISHSSHUSER: "tech",
  
    ENABLE_EXPORT_PG_TO_REDIS: true,
    ENABLE_REDIS: true,
    ENABLE_ES: true,
    FEATURES: {
      ENABLE_REPLY_REVIEW: true,
      ENABLE_SYN_SQL: true,
      LOGIN_DEP: true
    },
    REDIS_DRIVER: 'redis',// 'redis','ioredis'
    CRSF: false,
    TIME_EX_TOKEN:5,
    VERY_OTP_BESTPRICE:true,
    TIME_EX_TOTP:5,
    LIST_HOTEL:[1],//70,92,504,2850
    IS_RELOAD: true,
    SHOW_RATE_BY_MEMBER_LEVEL: true

  };


 
  // MySQL
  _config.MySQL = {
    getConfig: function (db, needWrite, returnArray) {
      var _settings = [];
      if (db == null || db == "" || db == undefined || db == "product" || db == "products") {
        db = "alontro_hscs";
      }
      if (needWrite) {
        var _setting = {
          host: MySQL_Master.host,
          port: MySQL_Master.port,
          user: MySQL_Master.user,
          password: MySQL_Master.password,
          database: db,
          charset: 'utf8_general_ci',
          timezone: TIME_ZONE_MYSQL,
          multipleStatements: true
        };
        _settings.push(_setting);
      }
      else {
        MySQL_Slave.forEach(function (_item) {
          var _setting = {
            host: _item.host,
            port: _item.port,
            user: _item.user,
            password: _item.password,
            database: db,
            charset: 'utf8_general_ci',
            timezone: TIME_ZONE_MYSQL,
            multipleStatements: true
          };
          _settings.push(_setting);
        });
      }
      return (returnArray) ? _settings : _settings[0];
    }
  };

  _config.MySQLRep = {
    getConfig: function (db) {
      if (db == null || db == "" || db == undefined || db == "product" || db == "products") {
        db = "alontro_hscs";
      }
      var MySQL_SlaveConf = [];
      MySQL_Slave.forEach(function (_item) {
        let _setting = {
          host: _item.host,
          port: _item.port,
          username: _item.user,
          password: _item.password
        };
        MySQL_SlaveConf.push(_setting);
      });
      var MySQL_MasterConf = {
        host: MySQL_Master.host,
        port:MySQL_Master.port,
        username: MySQL_Master.user,
        password: MySQL_Master.password,
      };
      var settingTem = {
        dialect: 'mysql',
        replication: {
          read: MySQL_SlaveConf,
          write: MySQL_MasterConf
        },
        charset: 'utf8_general_ci',
        timezone: TIME_ZONE_MYSQL,
        syncOnAssociation: false,
        dialectOptions: {
          multipleStatements: true
        },
        pool: {}
      }
      return {database: db, configRep: settingTem};
    }
  };
  // connect only Master
  _config.MySQLMaster = {
    getConfig: function (db) {
      if (db == null || db == "" || db == undefined || db == "product" || db == "products") {
        db = "alontro_hscs";
      }
      const MySQL_MasterConf = {
        host: MySQL_Master.host,
        port: MySQL_Master.port,
        username: MySQL_Master.user,
        password: MySQL_Master.password,
      };
      const settingTem = {
        dialect: 'mysql',
        replication: {
          read: MySQL_MasterConf,
          write: MySQL_MasterConf
        },
        charset: 'utf8_general_ci',
        timezone: TIME_ZONE_MYSQL,
        syncOnAssociation: false,
        dialectOptions: {
          multipleStatements: true
        },
        pool: {}
      }
      return {database: db, settingMore: settingTem};
    }
  };
  // REDIS TRACKING
  _config.REDIS_TRACKING = {
    // getConfig: {
    //   port: Redis_Master.port,
    //   host: Redis_Master.host,
    //   db: Redis_Tracking.dbNumber
    // },
    // getSentinels: {
    //   sentinels: Redis_Tracking.sentinels,
    //   name: 'redis-cluster',
    //   port: Redis_Master.port,
    //   host: Redis_Master.host,
    //   db: Redis_Tracking.dbNumber
    // }
  };

  // REDIS
  _config.REDIS = {
    // getConfig: function (dbNumber, needWrite) {
    //   var _serverIP = Redis_Slave.host;
    //   var _serverPort = Redis_Slave.port;
    //   if (needWrite) {
    //     _serverIP = Redis_Master.host;
    //     _serverPort = Redis_Master.port;
    //   }

    //   if (dbNumber && dbNumber > 0)
    //     return 'redis://' + _serverIP + ':' + _serverPort + '/' + dbNumber;

    //   return 'redis://' + _serverIP + ':' + _serverPort;
    // },

    // getSentinels: function () {
    //   return Redis_Sentinels;
    // }
  };

  // RabbitMQ
  _config.RABBITMQ = {
    // getConfig: function (vHost, needWrite) {
    //   var _server = RabbitMQ_Slave;
    //   if (needWrite) {
    //     _server = RabbitMQ_Master;
    //   }
    //   var _connectionString = 'amqp://' + _server.user + ':' + _server.password + '@' + _server.host;
    //   if (vHost && vHost.length > 0) {
    //     _connectionString = 'amqp://' + _server.user + ':' + _server.password + '@' + _server.host + '/' + vHost;
    //   }
    //   console.log('RABBITMQ', '_connectionString', _connectionString);
    //   return _connectionString;
    // }
  };


  // ELASTICSEARCH
  _config.ELASTICSEARCH = {

    getConfig: function (needWrite) {
      var _serverIP = ES_Slave.host;
      if (needWrite) {
        _serverIP = ES_Master.host;
      }
      var __config = {
        hosts: _serverIP,
        log: 'error', // trace, error
        sniffOnStart: false, // default is false. Should the client attempt to detect the rest of the cluster when it is first instantiated? // https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/configuration.html
        requestTimeout: 600000 // default is 30000 ms = 30 seconds. 300000 ms = 5 mins. 600000 ms = 10 mins
      };
      console.log('elasticsearch', 'config', __config);
      return __config;
    },
    getConfigV7: function () {
      const _serverIP = ES_Query_V7.host;
      const __config = {
        nodes: _serverIP,
        maxRetries: 5,
        requestTimeout: 600000 // default is 30000 ms = 30 seconds. 300000 ms = 5 mins. 600000 ms = 10 mins
      };
      return __config;
    }
  };

  _config.ONEPAY =
    {
      
    }

  // EMAIL
  _config.SMTP_EMAIL = {

    getConfig: function (provider) {


      switch (provider) {
        case 'local':
          return {
            host: 'ste..com',
            port: 24455,
            secure: false
          }
        case 'sendgrid':
          return {
            host: 'ste..com',
            port: 24455,
            secure: false
          }

        case 'mailjet':
          return {
            host: 'ste..com',
            port: 24455,
            secure: false
          }
      }

      return {
        host: 'ste..com',
        port: 24455,
        secure: false
      }
    }
  }


  // URL
  _config.ROOT_URL =
    {
    
      _ROOT_URLS: 'http://..com',
      ROOT_URLS: 'http://www..com',
      APIN_ROOT_URLS: 'https://apin..com',
      API_ROOT_URLS: 'http://:3300'
    }


  // Email
  _config.EMAIL =
    {
      
    }


  _config.SERVICE_API =
    {
      SLACK: {
        URL: 'https://hooks.slack.com/services/T0NL3L69Z/B1RMQGZEK/',
        ENABLED: false
      },
      MAIL_CHIMP: {
        MAIL_CHIMP_API_ROOTURL: 'https://us13.api.mailchimp.com/2.0/',
        MAIL_CHIMP_LIST_ID: '',
        MAIL_CHIMP_API_KEY: '-us13'
      },
      FACEBOOK_APP_API: ""
    }

}